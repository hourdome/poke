import java.util.Scanner;
import java.util.Random;
// ================TEST=================
public class pokemonGame {
    public static Status enemyName;
    // hp , def, atk, lvl
    // 0  ,  1 ,  2 ,  3
    public static int[] enemyStat = new int[5];
    public static String pokeNamePlayer = "Pickachu";
    // hp , def, atk, exp, lvl
    // 0  ,  1 ,  2 ,  3 ,  4    
    public static int[] pokeStatPlayer = new int[5];
    public static int maxHpPlayer;
    public static int maxHpEnemy;
    final static Random rand = new Random();
    final static Scanner Obj = new Scanner(System.in);

    public static void main(final String[] args) {
        // fix stat
        pokeStatPlayer[0] = 40 * 5 + pokeStatPlayer[4]; //hp
        pokeStatPlayer[1] = 45 * 2 + pokeStatPlayer[4]; //def
        pokeStatPlayer[2] = 60 * 2 + pokeStatPlayer[4]; //atk
        pokeStatPlayer[3] = 0; //exp
        pokeStatPlayer[4] = 5; //lvl
        maxHpPlayer = pokeStatPlayer[0];
        randMon();
        System.out.println("✦ ✧ ✦ ✧ ✦ ✧  POKEMON BATTE  ✦ ✧ ✦ ✧ ✦ ✧ ");
        boolean start = true;
        while (start) {
            System.out.println();
            System.out.println("============================");
            System.out.println("Your Pokémon : " + pokeNamePlayer);
            System.out.println("LEVEL: "+ pokeStatPlayer[4]); 
            System.out.println("HP: " + pokeStatPlayer[0]);
            System.out.println();
            System.out.println("Enemy Pokémon : " + enemyName);
            System.out.println("LEVEL: " + enemyStat[3]);
            System.out.println("HP: " + enemyStat[0]);
            System.out.println("============================");
            System.out.println("Choose your action");
            System.out.println("1 : Attack\t\t2 : Skill-Thunder Shock");
            System.out.println("3 : Potion Heal\t\t4 : RUN!!!!");
            System.out.printf("Enter: ");
            final int mode = Obj.nextInt();
            System.out.println("============================");
            delay(3); // x second
            if(mode == 1){
                PAction();
                delay(3);
                EnAction();
                delay(3);
            }
            else if(mode == 2){
                PAction2();
                delay(3);
                EnAction();
                delay(3);
            }
            else if(mode == 3){
                PAction3();
                delay(3);
                EnAction();
                delay(3);
            }
            else if(mode == 4){
                PAction4();
                delay(3);
                EnAction();
                delay(3);
                
            }

            if(pokeStatPlayer[0] <= 0){
                System.out.println();
                System.out.println("============================");
                System.out.println("!! Your Pokemon Death !!");
                System.out.println("============================");
                System.out.println("Do you want to play again?");
                System.out.println("1 : Play Again\t\t2 : Quit");
                System.out.printf("Enter: ");
                final int mode2 = Obj.nextInt();
                if(mode2 == 1){
                    randMon();
                }
                else{
                    System.out.println("============================");
                    System.out.println();
                    System.out.println("❤ ❤ ❤ THX to play ❤ ❤ ❤");
                    System.out.println();
                    start = false;
                }
            }
            if(enemyStat[0] <= 0){
                System.out.println();
                System.out.println("============================");
                System.out.println("!! Enemy Pokemon Death !!");
                System.out.println("============================");
                System.out.println("Do you want to play again?");
                System.out.println("1 : Play Again\t\t2 : Quit");
                System.out.printf("Enter: ");
                final int mode2 = Obj.nextInt();
                if(mode2 == 1){
                    randMon();
                }
                else{
                    System.out.println("============================");
                    System.out.println();
                    System.out.println("❤ ❤ ❤ THX to play ❤ ❤ ❤");
                    System.out.println();
                    start = false;
                }
            }

        }
            
            
        

    }


    private static void randMon() {
        enemyName = pokemon.randName();
        enemyStat = pokemon.randSecond();
        maxHpEnemy = enemyStat[0]; 
    }
    private static void delay(int x){
        System.out.print("Wait Seconds ");
        try{
            for(int i = 0; i < x; i++){
                Thread.sleep(1000);
                System.out.print("! ");
            }
            System.out.println();
        }
        catch(InterruptedException e){
            System.err.println(e.getMessage());
        }
    }

    private static void PAction(){
        System.out.println();
        int damageAttack = pokemon.attacked(pokeStatPlayer[2], enemyStat[1]);
                if(damageAttack > 0) {
                    System.out.println(pokeNamePlayer + " "+ damageAttack + " damages");
                    enemyStat[0] = enemyStat[0] - damageAttack;
                }
                else {
                    System.out.println(pokeNamePlayer + " miss!!!");
                }
                System.out.println();
    }

    private static void PAction2(){
        System.out.println();
        int damageAttack = pokemon.skill(pokeStatPlayer[2], enemyStat[1]);
                if(damageAttack > 0) {
                    System.out.println(pokeNamePlayer + " skill "+ damageAttack + " damages");
                    enemyStat[0] = enemyStat[0] - damageAttack;
                }
                else {
                    System.out.println(pokeNamePlayer + " skill miss!!!");
                }
                System.out.println();
    }

    private static void PAction3(){
        System.out.println();
        int heal = pokemon.heal();
        pokeStatPlayer[0] = pokeStatPlayer[0] + heal;
        if(pokeStatPlayer[0] > maxHpPlayer){
            pokeStatPlayer[0] = maxHpPlayer;
        }
        System.out.println(pokeNamePlayer+ " use Potion "+ heal +" hp.");
        System.out.println();
    }

    private static void PAction4(){
        int esc = pokemon.escape(); 
        //System.out.println(esc);
        System.out.println();
        if(esc >= 50){
            delay(5);
            System.out.println();
            System.out.println(pokeNamePlayer + " RUN!!!");
            System.out.println();
            
            randMon();

        }else{
            delay(5);
            System.out.println();
            System.out.println(pokeNamePlayer + " RUN FAILED");
            System.out.println();
        }
        
    }

    private static void EnAction(){
        if(pokemon.EnemyAction() == 1){
            System.out.println();
            int enemyAttack = pokemon.attacked(enemyStat[2], pokeStatPlayer[1]);
            if(enemyAttack > 0) {
                System.out.println(enemyName + " skill "+ enemyAttack + " damages");
                pokeStatPlayer[0] = pokeStatPlayer[0] - enemyAttack;
            }
            else {
                System.out.println(enemyName + " miss!!!");
            }
            System.out.println();
            
        }
        else if(pokemon.EnemyAction() == 2){
            System.out.println();
            int enemyAttack = pokemon.skill(enemyStat[2], pokeStatPlayer[1]);
            if(enemyAttack > 0) {
                System.out.println(enemyName + " Skill "+ enemyAttack + " damages");
                pokeStatPlayer[0] = pokeStatPlayer[0] - enemyAttack;
            }
            else {
                System.out.println(enemyName + " skill miss!!!");
            }
            System.out.println();
        }
        else if(pokemon.EnemyAction() == 3){
            System.out.println();
            int heal = pokemon.heal();
            enemyStat[0] = enemyStat[0] + heal;
            if(enemyStat[0] > maxHpEnemy){
                enemyStat[0] = maxHpEnemy;
            }
            System.out.println(enemyName + " use Berry "+ heal +" hp.");
            System.out.println();

        }
        else if(pokemon.EnemyAction() == 4){
            System.out.println();
            if(pokemon.escape() >= 50){
                delay(5);
                System.out.println();
                System.out.println(enemyName + " RUN!!!");   
            }else{
                delay(5);
                System.out.println();
                System.out.println(enemyName + " RUN FAILED");
            }

        }
    }
    

    
}
// Damage=((((((((2*Level/5)+2)*Final(S.)AttackStat*BasePower/50)/Final(S.)DefenseStat)*Mod1)+2)*CH*RandomNumber/100*Mod2)*Mod3