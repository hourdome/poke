import java.util.Random;

enum Status {
    // hp ,def, atk
    Bulbasaur("Bulbasaur", 45, 49, 49, 0),
    Charmander("Charmander", 39, 43, 52, 0),
    Squirtle("Squirtle", 44, 65, 48, 0),
    Pikachu("Pikachu", 35, 40, 55, 0),
    Pidgeotto("Pidgeotto", 63, 55, 60, 0),
    Rattata("Rattata", 30, 35, 56, 0),
    Ekans("Ekans", 35, 44, 60, 0),
    Meowth("Meowth", 40, 35, 45, 0),
    Psyduck("Psyduck", 50, 48, 52, 0),
    Magikarp("Magikarp", 20, 55, 10, 0);

    private final String Names;
    private final int hp;
    private final int def;
    private final int atk;
    private final int lvl;
    
    Status(final String NAMES, final int heath, final int defence, final int Attack, final int Level) {
        this.Names = NAMES;
        this.hp = heath;
        this.def = defence;
        this.atk = Attack;
        this.lvl = Level;
    }
    static Random ran = new Random();
    public int getHp(){ return hp * 5 + StatByLvl_Base() + (int)(Math.random()*21); }

    public int getDef(){ return def * 2 + StatByLvl_Base() +(int)(Math.random()*6); }
    
    public int getAtk(){ return atk * 2 + StatByLvl_Base() + (int)(Math.random()*11); }

    public int getLvl() { return lvl + StatByLvl_Base(); }

    public String getNames(){ return Names; }

    public static int StatByLvl_Base(){ // Status By Level
        
        int randlvl = (int)(Math.random()*(15-5)+5);
        
        return randlvl;
        
    }

}

// random Status
class RandomPoke<E extends Enum<Status>>{
    Random rand = new Random();
    E[] values;

    public RandomPoke(final Class<E> token) {
        values = token.getEnumConstants();
    }

    public E randomN() {
        return values[rand.nextInt(values.length)];
    }
}

// random pokemon
public class pokemon{
    private static Status enemy_N;
    private static int[] enemy_S = new int[5];
    static Random rand = new Random();
    public static Status randName() {
        final RandomPoke<Status> r = new RandomPoke<Status>(Status.class);
        enemy_N = r.randomN();
        randSecond();
        return enemy_N; 
    }

    static int[] randSecond() {
        
        if(enemy_N == Status.Bulbasaur) {enemy_S[0] = Status.Bulbasaur.getHp(); enemy_S[1] = Status.Bulbasaur.getDef(); enemy_S[2] = Status.Bulbasaur.getAtk(); enemy_S[3] = Status.Bulbasaur.getLvl();}
        else if(enemy_N == Status.Charmander){enemy_S[0] = Status.Charmander.getHp(); enemy_S[1] = Status.Charmander.getDef(); enemy_S[2] = Status.Charmander.getAtk(); enemy_S[3] = Status.Charmander.getLvl();}
        else if(enemy_N == Status.Squirtle){enemy_S[0] = Status.Squirtle.getHp(); enemy_S[1] = Status.Squirtle.getDef(); enemy_S[2] = Status.Squirtle.getAtk(); enemy_S[3] = Status.Squirtle.getLvl();}
        else if(enemy_N == Status.Pikachu){enemy_S[0] = Status.Pikachu.getHp(); enemy_S[1] = Status.Pikachu.getDef(); enemy_S[2] = Status.Pikachu.getAtk(); enemy_S[3] = Status.Pikachu.getLvl();}
        else if(enemy_N == Status.Pidgeotto){enemy_S[0] = Status.Pidgeotto.getHp(); enemy_S[1] = Status.Pidgeotto.getDef(); enemy_S[2] = Status.Pidgeotto.getAtk(); enemy_S[3] = Status.Pidgeotto.getLvl();}
        else if(enemy_N == Status.Rattata){enemy_S[0] = Status.Rattata.getHp(); enemy_S[1] = Status.Rattata.getDef(); enemy_S[2] = Status.Rattata.getAtk(); enemy_S[3] = Status.Rattata.getLvl();}
        else if(enemy_N == Status.Ekans){enemy_S[0] = Status.Ekans.getHp(); enemy_S[1] = Status.Ekans.getDef(); enemy_S[2] = Status.Ekans.getAtk(); enemy_S[3] = Status.Ekans.getLvl();}
        else if(enemy_N == Status.Meowth){enemy_S[0] = Status.Meowth.getHp(); enemy_S[1] = Status.Meowth.getDef(); enemy_S[2] = Status.Meowth.getAtk(); enemy_S[3] = Status.Meowth.getLvl();}
        else if(enemy_N == Status.Psyduck){enemy_S[0] = Status.Psyduck.getHp(); enemy_S[1] = Status.Psyduck.getDef(); enemy_S[2] = Status.Psyduck.getAtk(); enemy_S[3] = Status.Psyduck.getLvl();}
        else if(enemy_N == Status.Magikarp){enemy_S[0] = Status.Magikarp.getHp(); enemy_S[1] = Status.Magikarp.getDef(); enemy_S[2] = Status.Magikarp.getAtk(); enemy_S[3] = Status.Magikarp.getLvl();}

        return enemy_S;
    
    }
    // yourAtk , oppDef
    public static int attacked(final int x, final int y) {
        int a;
        final int miss = (int)(Math.random()*100);
        //System.out.println("missing: "+ miss);
        if (miss > 25) {
            a = (((x / 3) - ((y / 2) % 1 * ((int)(Math.random()*10)))));
        } else {
            return 0;
        }
        return a;

    }

    public static int skill(final int x, final int y) {
        int a;
        a = ((x * 2) - y) / 2;
        if (a < 0) {
            a = a * (-1);
        }
        return a;
    }

    public static int heal() {      
        int  r = (int)(Math.random()*(30-20)+20);   
        return r;
    }

    public static int escape() {
        return rand.nextInt(100) + 1;
    }

    public static int EnemyAction(){
        
        return (int)(Math.random()*5);
        
       
    }

  
}